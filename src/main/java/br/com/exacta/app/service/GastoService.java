package br.com.exacta.app.service;

import br.com.exacta.app.converter.DozerConverter;
import br.com.exacta.app.dto.GastoDto;
import br.com.exacta.app.entity.Gasto;
import br.com.exacta.app.exception.ResourceNotFoundException;
import br.com.exacta.app.repository.GastoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GastoService {

    @Autowired
    private GastoRepository gastoRepository;

    public List<GastoDto> obterTodosOsGastos() {
        return DozerConverter.parseListObject(gastoRepository.findAll(), GastoDto.class);
    }

    public GastoDto createGasto(GastoDto gastoDto) {
        var entity = DozerConverter.parseObject(gastoDto, Gasto.class);
        return DozerConverter.parseObject(gastoRepository.save(entity), GastoDto.class);
    }

    public GastoDto findById(Long id) {
        var entity = gastoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("No records found for this ID"));
        return DozerConverter.parseObject(entity, GastoDto.class);
    }


}
