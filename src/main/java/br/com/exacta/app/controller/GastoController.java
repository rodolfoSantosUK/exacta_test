package br.com.exacta.app.controller;


import br.com.exacta.app.dto.GastoDto;
import br.com.exacta.app.service.GastoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("exacta/gastos")
public class GastoController {

    @Autowired
    private GastoService gastoService;

    @GetMapping
    public List<GastoDto> findAll() {

        return gastoService.obterTodosOsGastos();
    }

    @PostMapping
    public GastoDto save(@RequestBody GastoDto gastoDto) {
        return gastoService.createGasto(gastoDto);
    }

    @GetMapping("/{id}")
    public GastoDto findById(@PathVariable("id") Long id) {
        return gastoService.findById(id);
    }



}
