package br.com.exacta.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExactaTesteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExactaTesteApplication.class, args);
	}

}
