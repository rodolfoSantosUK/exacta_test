package br.com.exacta.app.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GastoDto {

    private Long   id;
    private String nomePessoa;
    private String  descricao;
    private String horario;
    private String tags;

}
