package br.com.exacta.app.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class Gasto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long   id;
    private String nomePessoa;
    private String  descricao;
    private String horario;
    private String tags;

}



